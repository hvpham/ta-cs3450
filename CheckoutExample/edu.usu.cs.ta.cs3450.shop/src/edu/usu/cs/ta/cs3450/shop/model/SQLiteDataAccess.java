package edu.usu.cs.ta.cs3450.shop.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLiteDataAccess implements IDataAccess {
	private static final String DATABASE_PATH = "database/database.db";

	private final Connection mConn;

	public SQLiteDataAccess() throws ClassNotFoundException {
		Class.forName("org.sqlite.JDBC");
		mConn = connectToDatabase();
	}

	private Connection connectToDatabase() {
		Connection conn = null;
		try {
			// db parameters
			String url = "jdbc:sqlite:" + DATABASE_PATH;
			// create a connection to the database
			conn = DriverManager.getConnection(url);

			System.out.println("Connection to SQLite has been established.");

			return conn;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@Override
	public Product getProductById(int id) {
		String sql = "SELECT * FROM PRODUCT WHERE ID=?";

		PreparedStatement pstmt;
		try {
			pstmt = mConn.prepareStatement(sql);
			pstmt.setInt(1, id);

			ResultSet rs = pstmt.executeQuery();

			// should be only one result
			if (rs.next()) {
				return new Product(rs.getInt("ID"), rs.getString("Name"), rs.getDouble("UNITPRICE"));
			} else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
