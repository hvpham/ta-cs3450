package edu.usu.cs.ta.cs3450.shop.model;

public interface IDataAccess {
	public Product getProductById(int id);
}
