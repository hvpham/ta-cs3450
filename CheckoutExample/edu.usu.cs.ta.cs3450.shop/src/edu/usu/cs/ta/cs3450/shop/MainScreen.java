package edu.usu.cs.ta.cs3450.shop;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import edu.usu.cs.ta.cs3450.shop.checkout.CheckoutScreen;

public class MainScreen extends Screen {
	// View
	JButton mCheckOutButton;
	JButton mInvetoryButton;

	public MainScreen(JFrame frame) {
		super(frame);

		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		addPanel(mainPanel);

		JLabel title = new JLabel("Shop assistance system");
		title.setFont(new Font("Serif", Font.BOLD, 22));
		title.setMaximumSize(new Dimension(Integer.MAX_VALUE, title.getMinimumSize().height));
		mainPanel.add(title);

		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		mainPanel.add(panel);

		mCheckOutButton = new JButton("Checkout");
		mCheckOutButton.setPreferredSize(new Dimension(200, 100));
		mCheckOutButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				removePanel(mainPanel);
				new CheckoutScreen(frame);
			}
		});
		panel.add(mCheckOutButton);

		mInvetoryButton = new JButton("Inventory");
		mInvetoryButton.setPreferredSize(new Dimension(200, 100));
		mInvetoryButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(frame, "This functionality has not been implemented yet!!!");
			}
		});
		panel.add(mInvetoryButton);

		// Display the window.
		frame.pack();
		frame.setVisible(true);
	}
}
