package edu.usu.cs.ta.cs3450.shop.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLiteInit {
	private static final String DATABASE_PATH = "database/database.db";

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		Class.forName("org.sqlite.JDBC");
		// Create Connection
		Connection conn = DriverManager.getConnection("jdbc:sqlite:" + DATABASE_PATH);
		Statement stmt = null;

		try {
			String sql = "CREATE TABLE IF NOT EXISTS PRODUCT ("//
					+ "ID INTEGER PRIMARY KEY AUTOINCREMENT,"//
					+ "NAME           TEXT    NOT NULL,"//
					+ "UNITPRICE      REAL    NOT NULL);";
			stmt = conn.createStatement();
			stmt.execute(sql);

			sql = "DELETE FROM PRODUCT;";

			stmt = conn.createStatement();
			stmt.execute(sql);

			sql = "INSERT INTO PRODUCT(NAME,UNITPRICE) VALUES(?,?)";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, "Apple");
			pstmt.setDouble(2, 3.99);
			pstmt.executeUpdate();

			pstmt.setString(1, "Apple Pie");
			pstmt.setDouble(2, 5.99);
			pstmt.executeUpdate();

			pstmt.setString(1, "Apple Juice");
			pstmt.setDouble(2, 2.99);
			pstmt.executeUpdate();

			pstmt.setString(1, "Apple Watch");
			pstmt.setDouble(2, 399.99);
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
